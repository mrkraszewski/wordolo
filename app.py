from flask import Flask
import requests
from bs4 import BeautifulSoup as bs
from collections import Counter
app = Flask(__name__)

stopwords = [a.strip().lower() for a in open('stopwords.txt','r').readlines()if a]

@app.route('/')
def hello_world():
    return '''<h1>Greetings</h1>
<p>The purpose of this page is to say, <span>Hello World</span> to the world! :)</p>'''

@app.route('/<mystring>')
def profile(mystring):
    return f'''<h1>Greetings {mystring.title()}!</h1>
<p>The purpose of this page is to say, <span>Hello World</span> to the world! :)</p>'''

@app.route('/headers/<myurl>')
def headers(myurl):
    if myurl:
        return str(get_response(myurl).headers)
    else:
        return "Please enter a URL to fetch after `/getit/`"

@app.route('/topwords/<path:myurl>')
def topwords(myurl):
    if myurl:
        return get_top_words(myurl)
    else:
        return "Please enter a URL to fetch after `/topwords/`"

def get_response(url):
    response = requests.get("http://www." + url)
    return response

def get_top_words(url):
    all_words = []
    raw = get_response(url).content
    soup = bs(raw, 'html.parser')
    content = [a for a in soup.find_all() if a.name in ["p","a","ul","ol","h1","h2","h3","section"]]

    chunks = [a.text for a in content]
    for chunk in chunks:
        for word in chunk.split(" "):
            if word.lower() not in stopwords and word.lower() != "":
                all_words.append(word.lower())
    result = f"Top Words for: {url}"
    for word, count in Counter(all_words).most_common(10):
        result += f"<p>{word}   {count}</p>"
    return result

